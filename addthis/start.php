<?php

/**
 * Elgg addthis plugin
 */

// TODO Use metatags for better config of addthis
// LINK http://stackoverflow.com/questions/7127578/addthis-changing-description-title-and-url-being-sent

/**
 *
 * Addthis bar options
 *
 * LARGE
 * <div class="addthis_bar addthis_bar_vertical addthis_bar_large" style="top:50px;left:50px;">
            <div class="addthis_toolbox addthis_default_style">
 *
 * MEDIUM
 * <div class="addthis_bar addthis_bar_vertical addthis_bar_medium" style="top:50px;left:150px;">
            <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
 *
 * SMALL
 * <div class="addthis_bar addthis_bar_vertical addthis_bar_small" style="top:50px;left:220px;">
            <div class="addthis_toolbox addthis_default_style">
 */

include(dirname(__FILE__) . '/lib/lib.php');

function addthis_init() {
	// Load customized css
	elgg_extend_view('css/elgg', 'addthis/css');

    $position = elgg_get_plugin_setting('position', 'addthis');
    switch($position) {
        case 'left':
            $show_left = true;
            break;
        case 'menu':
            $show_menu = true;
            break;
        case 'both':
            $show_left = true;
            $show_menu = true;
            break;
        case 'none':
        default:
            $show_left = false;
            $show_menu = false;
            break;
    }

    // Show addthis share bar in header
    if($show_menu) {
        elgg_extend_view('page/elements/header','addthis/addthis_header');
    }

    // Show addthis share bar on left
    if($show_left) {
        elgg_extend_view('page/elements/foot','addthis/addthis_left');
    }

    // Extend footer to load adthis bar (can be a long js load, so load later)
    if($show_left || $show_menu) {
        elgg_extend_view('page/elements/foot', 'addthis/addthis_js', 1000);
    }
}

elgg_register_event_handler('init', 'system', 'addthis_init');
?>