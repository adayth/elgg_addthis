<?php
    $label = elgg_echo('addthis:urldescription') . ' ra-4e9d4d2a4d79ff43';
    $options = array(
        'name' => 'params[addthisurl]',
        'value' => elgg_get_plugin_setting('addthisurl', 'addthis'),
    );
    $control = elgg_view('input/text', $options);
?>

<div>
    <label><?php echo $label; ?></label>
    </br>
    <?php echo $control; ?>
</div>

<?php
    $label = elgg_echo('addthis:positiondescription');
    $options = array(
        'name' => 'params[position]',
        'value' => elgg_get_plugin_setting('position', 'addthis'),
        'options_values' => array(
            'none' => elgg_echo('addthis:position:none'),
            'left' => elgg_echo('addthis:position:left'),
            'menu' => elgg_echo('addthis:position:menu'),
            'both' => elgg_echo('addthis:position:both'),
        ),
    );
    $control = elgg_view('input/dropdown', $options);
?>

<div>
    <label><?php echo $label; ?></label>
    </br>
    <?php echo $control; ?>
</div>