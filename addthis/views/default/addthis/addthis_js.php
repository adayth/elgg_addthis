<?php

// Don't load share bar in admin section
if (elgg_get_context() == 'admin') {
	return;
}

$addthis_url = elgg_get_plugin_setting('addthisurl', 'addthis');
if(!$addthis_url) $addthis_url = "";

?>

<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=<?php echo $addthis_url; ?>"></script>
