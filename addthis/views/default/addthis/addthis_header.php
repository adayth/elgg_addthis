<?php
/**
 * Elgg addthis plugin
 */

?>

<div style="position: absolute; right: 250px; bottom: 1px;">
    <div class="addthis_toolbox addthis_default_style">
        <a class="addthis_button_facebook"></a>
        <a class="addthis_button_twitter"></a>
        <a class="addthis_button_google_plusone" g:plusone:count="false" g:plusone:annotation="none"></a>
        <a class="addthis_button_diigo"></a>
        <a class="addthis_button_wordpress"></a>
        <a class="addthis_button_compact"></a>
    </div>
</div>