<?php
/**
 * Elgg addthis plugin
 */

// Don't show the bar in mobile devices
if(!detect_mobile()) {
?>

<div class="addthis_bar addthis_bar_vertical addthis_bar_medium">
	<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
        <a class="addthis_button_facebook"></a>
        <a class="addthis_button_twitter"></a>
        <a class="addthis_button_google_plusone" g:plusone:count="false" g:plusone:annotation="none"></a>
        <a class="addthis_button_diigo"></a>
        <a class="addthis_button_wordpress"></a>
        <a class="addthis_button_compact"></a>
	</div>
</div>

<?php
}
?>