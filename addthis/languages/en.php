<?php
/**
 * Addthis English language file.
 */

$english = array(
    'addthis:urldescription' => "Put here your addthis user ID for sharing analytics generation, for example",
    'addthis:positiondescription' => "Select a position to show addthis share bar",
    'addthis:position:none' => "Don't show the bar",
    'addthis:position:left' => "Show a vertical bar floating on left at the screen",
    'addthis:position:menu' => "Show a horizontal bar integrated in main menu",
    'addthis:position:both' => "Show horizontal and vertical bar",
);

add_translation('en', $english);

?>
