<?php

$language = array(
    'addthis:urldescription' => "Pon aquí tu ID de usuario de addthis para generar las estadística de compartición, por ejemplo",
    'addthis:positiondescription' => "Selecciona una posición para mostrar la barra de addthis",
    'addthis:position:none' => "No mostrar la barra",
    'addthis:position:left' => "Mostrar una barra vertical flotante en la izquierda de la ventana",
    'addthis:position:menu' => "Mostrar una barra horizontal en el menú principal",
    'addthis:position:both' => "Mostrar la barra vertial y horizontal",
);
add_translation("es", $language);
